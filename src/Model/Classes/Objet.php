<?php
namespace ProjetAnimalerie\Model\Classes;

use \Exception;

abstract class Objet
{
    public function __set($property, $value)
    {
        $class_name = get_class($this);

        if (! property_exists($class_name, $property) )
        {
            throw new Exception("Property $property not found in class $class_name", 1);
        }

        $method_name = "set_" . $property;

        if (! method_exists($class_name, $method_name))
        {
            throw new Exception("Cannot set property $property in class $class_name", 1);
        }

        $this->$method_name($value);
    }

    public function __get($property)
    {
        $class_name = get_class($this);

        if (! property_exists($class_name, $property) )
        {
            throw new Exception("Property $property not found in class $class_name", 1);
        }

        $method_name = "get_" . $property;

        if (! method_exists($class_name, $method_name))
        {
            throw new Exception("Cannot get property $property in class $class_name", 1);
        }

        return $this->$method_name();
    }
    
}